# include "monitor.h"

FILE * readFile(char *strPath,FILE *fpReadFile,int *nNewFileSize)
{
  int nNewFileSizeTemp=0;
  FILE *fpRead=NULL;
  struct stat pStat; 

  if(access(strPath,F_OK)==0)
  {
    stat (strPath,&pStat);
    nNewFileSizeTemp=(int)pStat.st_size; 
  }
  *nNewFileSize=nNewFileSizeTemp;
  if(fpReadFile!=NULL)
    return fpReadFile;
  while(1)
  {
    fpRead=fopen(strPath,"r");
    if(fpRead!=NULL)
    {
      if(fseek(fpRead,0L,SEEK_SET)<0)
      {
        fprintf (stderr,"%d %s\n",errno,strerror(errno));
        return 0;
      }
      return fpRead;
    }
  }
}
