# include "monitor.h"

int checkLessNumberSend(FILE *fpRead,int nNumber,int *nSizeOfFileOld,int *nSizeOfFile)
{
  int nTokenCount,nFlag=0;
  char *strBuffer,**strToken;

  if(*nSizeOfFileOld<*nSizeOfFile && *nSizeOfFile > 0)
  {
    if((strBuffer=(char *)malloc(sizeof(char)*MAX_BUFFER))==NULL)
    {
      fprintf(stderr,"Memory Allocation Failed\n");
      return -1;
    }

    fgets(strBuffer,MAX_BUFFER,fpRead);
    tokenise(strBuffer,&strToken,&nTokenCount);
    free(strBuffer);
    if( (atoi(strToken[0])==200) && ((atoi(strToken[2])+1)==nNumber) )
      nFlag=1;
    for(nTokenCount=0;nTokenCount<MAX_TOKEN_COUNT;nTokenCount++)
      free(strToken[nTokenCount]);
    free(strToken);
    if(nFlag==1)
      return 0;
  }
  return -1;
}
