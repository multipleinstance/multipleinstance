# include "monitor.h"

int writeLog(FILE *fpWrite,char strMsgCode[MIN_STRING_LEN],char strMsgName[MIN_STRING_LEN],char strSendMsg[MAX_BUFFER],char strComment[MAX_BUFFER])
{
  fputs(strMsgCode,fpWrite);
  fputc(' ',fpWrite);
  fputs(strMsgName,fpWrite);
  fputc(' ',fpWrite);
  fputs(strSendMsg,fpWrite);
  fputc(' ',fpWrite);
  fputs(strComment,fpWrite);
  fputc('\n',fpWrite);
  fflush(fpWrite);
  return 0;
}
