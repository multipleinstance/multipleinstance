# include "monitor.h"

void * aliveSendMsg(void *ptInfo)
{
  ThreadInfo *tInfo=ptInfo;
  fprintf(stdout,"%d %s %d %p\n",tInfo->nSocketFd,tInfo->strMsg,tInfo->nSizeofSockUdp,(void *)tInfo->sockUdp);
  if(connect(tInfo->nSocketFd,(const struct sockaddr *)tInfo->sockUdp,tInfo->nSizeofSockUdp)<0) {
    fprintf(stderr,"In aliveSendMsg1 Connect:errno %d error name %s\n",errno,strerror(errno));
    return 0;
  }
  while(1) {
  if(sendto((int)tInfo->nSocketFd,tInfo->strMsg,MIN_STRING_LEN,0,(const struct sockaddr *)tInfo->sockUdp,tInfo->nSizeofSockUdp)<0) {
    fprintf(stderr,"In aliveSendMsg1:errno %d error name %s\n",errno,strerror(errno));
    return 0;
  }
  sleep(tInfo->nSleepAlive);
  }
  return((void *)0);
}
