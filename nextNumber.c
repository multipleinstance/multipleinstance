# include "monitor.h"

int main(int argc,char **argv)
{
  int nSocketFdEven,nSocketFdAlive,nNumber,nEndNumber,nTokenCount;
  char strBuffer[MAX_BUFFER],strBufferLine[MAX_CHARS_IN_LINE],**strToken,*strStringInt,*strDelay;
  struct sockaddr_in sock_Even,sockUdpAlive;
  FILE *fpWrite,*fpRead=NULL;
  ThreadInfo tInfo;
  pthread_t ptNo;
  memset(&strBuffer,0,MAX_BUFFER);
  if(argc<2)
  {
    fprintf(stderr,"Pleasea give input like this\n ./a.out config_file\n");
    return 0;
  }
  fpRead=fopen(argv[1],"r");
  fgets(strBuffer,MAX_BUFFER,fpRead);
  tokenise(strBuffer,&strToken,&nTokenCount); 
  nNumber=atoi(strToken[3]);
  nEndNumber=atoi(strToken[4]);
  if((strDelay=(char *)malloc(sizeof(char)*MIN_STRING_LEN))==NULL) 
  {
    fprintf(stderr,"Memory allocation failed\n");
    return 0;
  }
  strcpy(strDelay,strToken[5]);
  createSocket(&nSocketFdEven,&sock_Even,strToken[0],strToken[1],TCP_CONNECTION,CLIENT);
  createSocket(&nSocketFdAlive,&sockUdpAlive,strToken[0],strToken[2],UDP_CONNECTION,CLIENT);
  if(access(strToken[6],F_OK)!=0)
  {
    fpWrite=fopen(strToken[6],"a");
    fputs(strBuffer,fpWrite);
    fputc('\n',fpWrite);
    fflush(fpWrite);
  }
  else
  {
    fpRead=fopen(strToken[6],"r");
    /* we have go last line of log_file */
    rewind(fpRead);
    fseek(fpRead,-30,SEEK_END);


      fgets(strBufferLine,MAX_CHARS_IN_LINE,fpRead);
 /*     for(i=0;i<MAX_CHARS_IN_LINE;i++)
      {
        nIndexOfBuffer=0;
        while(strBufferLine[i]!='\n' && i<MAX_CHARS_IN_LINE)
        {
          strBuffer[nIndexOfBuffer++]=strBufferLine[i++];
        }
      }*/
      fpWrite=fopen(strToken[6],"a");
    tokenise(strBufferLine,&strToken,&nTokenCount);  
    nNumber=atoi(strToken[2]);
    nNumber++;
    writeLog(fpWrite,"210","READ_LOG_START_AGAIN",strToken[2],"Nothing");
  
  }

   tInfo.nSocketFd=nSocketFdAlive;
   tInfo.sockUdp=&sockUdpAlive;
   strcpy(tInfo.strMsg,argv[0]);
   tInfo.nSizeofSockUdp=sizeof(sockUdpAlive);
   tInfo.nSleepAlive=5;
   fprintf(stdout,"R:%d %s %d %p\n",nSocketFdAlive,argv[0],tInfo.nSizeofSockUdp,(void *)&sockUdpAlive);
  pthread_create(&ptNo,NULL,aliveSendMsg,&tInfo);
  while(nNumber<=nEndNumber)
  {
    usleep((unsigned int)atoi(strDelay));
    memset(&strBuffer,0,MAX_BUFFER);
    integerToString(nNumber,&strStringInt);
    send(nSocketFdEven,strStringInt,MAX_BUFFER,0);
    writeLog(fpWrite,"200","SEND_MONITOR_NUM",strStringInt,"Nothing");
    nNumber++;
    free(strStringInt);
  }

  return 0;
}
