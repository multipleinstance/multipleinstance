# include "monitor.h"

int integerToString(int nNumber,char **strStoreAns)
{
  int nNumberTemp=0,nIndexBuffer=0;
  char *strBuffer,*strBufferTemp;
  strBuffer=(char *)malloc(sizeof(char)*MAX_BUFFER);
  strBufferTemp=(char *)malloc(sizeof(char)*MAX_BUFFER);
  nNumberTemp=nNumber;
  while(nNumberTemp>9)
  {
    strBuffer[nIndexBuffer++]=(nNumberTemp%10)+48;
    nNumberTemp/=10;
  }

    strBuffer[nIndexBuffer++]=nNumberTemp+48;
    strBuffer[nIndexBuffer--]='\0';
   for(nNumberTemp=0;nIndexBuffer>=0;nIndexBuffer--,nNumberTemp++)
     strBufferTemp[nNumberTemp]=strBuffer[nIndexBuffer];
   strBufferTemp[nNumberTemp]='\0';
  *strStoreAns=strBufferTemp;
  return 0;
}
