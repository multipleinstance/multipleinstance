all:libmonitor.a monitor ui nextNumber
CC=gcc
CFLAGS=-Wall -Werror -Wextra -pedantic -g3
RM = rm -f
# -ansi does allow use usleep() it will say implicit declaration 

libmonitor.a:CreateSocket.o  Tokenise.o IntegerToString.o CheckLessNumberSend.o ReadFile.o  WriteLog.o SignalHandling.o RelaunchProcess.o  AliveSendMsg.o  ExecuteProcess.o  
	ar -cvq libmonitor.a  CreateSocket.o Tokenise.o  IntegerToString.o CheckLessNumberSend.o ReadFile.o  WriteLog.o SignalHandling.o RelaunchProcess.o  AliveSendMsg.o  ExecuteProcess.o  
	$(RM) *.o


CreateSocket.o:CreateSocket.c
	$(CC) $(CFLAGS) -c CreateSocket.c

Tokenise.o:Tokenise.c
	$(CC) $(CFLAGS) -c Tokenise.c

IntegerToString.o:IntegerToString.c
	$(CC) $(CFLAGS) -c IntegerToString.c

CheckLessNumberSend.o:CheckLessNumberSend.c
	$(CC) $(CFLAGS) -c CheckLessNumberSend.c

ReadFile.o:ReadFile.c
	$(CC) $(CFLAGS) -c ReadFile.c

WriteLog.o:WriteLog.c
	$(CC) $(CFLAGS) -c WriteLog.c

SignalHandling.o:SignalHandling.c
	$(CC) $(CFLAGS) -c SignalHandling.c

RelaunchProcess.o:RelaunchProcess.c
	$(CC) $(CFLAGS) -c RelaunchProcess.c

AliveSendMsg.o:AliveSendMsg.c
	$(CC) $(CFLAGS) -c AliveSendMsg.c

ExecuteProcess.o:ExecuteProcess.c
	$(CC) $(CFLAGS) -c ExecuteProcess.c



nextNumber:nextNumber.c  
	  	  $(CC) $(CFLAGS) -o nextNumber nextNumber.c  libmonitor.a -pthread
	  	  $(CC) $(CFLAGS) -o nextNumber1 nextNumber.c  libmonitor.a -pthread
	  	  $(CC) $(CFLAGS) -o nextNumber2 nextNumber.c  libmonitor.a -pthread
	  	  $(CC) $(CFLAGS) -o nextNumber3 nextNumber.c  libmonitor.a -pthread
	  	  $(CC) $(CFLAGS) -o nextNumber4 nextNumber.c  libmonitor.a -pthread

ui:ui.c  
		  $(CC) $(CFLAGS) -o ui ui.c  libmonitor.a -pthread

monitor:monitor.c  
		  $(CC) $(CFLAGS) -o monitor monitor.c libmonitor.a -pthread

run:
	./monitor 127.0.0.1 5000 5001 monitor.log < input_monitor &
	./ui 127.0.0.1 5002 5000 < input_ui_1 &
	./ui 127.0.0.1 5003 5000 < input_ui_2 &
	./ui 127.0.0.1 5004 5000 < input_ui_3 &
	./ui 127.0.0.1 5005 5000 < input_ui_4 &
	./ui 127.0.0.1 5006 5000 < input_ui_5 &

runVal:
		valgrind --leak-check=full --leak-resolution=high --num-callers=40 ./nextEven nextEven.config &

clean:
	$(RM) monitor ui nextNumber nextNumber1 nextNumber2 nextNumber3 nextNumber4 libmonitor.a *.log *.config 

