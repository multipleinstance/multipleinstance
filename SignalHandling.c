#include "monitor.h"

int signalHandling(int nPidMonitor,int nPidKill,int nStatus,FILE *fpWriteLog)
{
  switch(nStatus)
  {
    case SIGKILL:
      kill(nPidKill,SIGKILL);
      fprintf(fpWriteLog,"205 PROCESS_KILL %d %d\n",nPidMonitor,nStatus);
      fflush(fpWriteLog);
      return -1;
      break;
    case SIGCHLD:
      fprintf(fpWriteLog,"500 WORK_DONE SIGNAL:%d BYE",nStatus);
      fflush(fpWriteLog);
      exit(0);
      break;
    case 4991:
    case SIGSTOP:
      kill(nPidMonitor,SIGCONT);
      fprintf(fpWriteLog,"510 PROCESS_COUNTINUE:%d BYE",nPidMonitor);
      fflush(fpWriteLog);
      break;
    case 0:
      fprintf(fpWriteLog,"500 WORK_DONE SIGNAL:%d BYE",nStatus);
      fflush(fpWriteLog);
      exit(0);

  }
  return 0;
}
