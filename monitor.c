# include "monitor.h"

int main(int argc,char **argv)
{
  int i,nSocketFdMonitor,nSocketFdMonitorUDP,nSocketFdClient[MAX_PROCESS_COUNT],nSocketFdSendUI[MAX_PROCESS_COUNT],nNoByteGet;/*,nPid[MAX_PROCESS_COUNT],nProcessId[MAX_PROCESS_COUNT],nStatus;*/
  char strBuffer[MAX_BUFFER],**strStartNo,**strEndNo,**strDelayNextNumber,**strPortServer,**strExecutableName,**strConfigFile,*strPathOfExecutable,strTemp[MIN_STRING_LEN];
  Process_Info processInfo[MAX_PROCESS_COUNT];
  struct sockaddr_in sock_Monitor,sock_MonitorUDP,sock_Client;
  FILE *fpWrite,*fpWriteLog;
  pthread_t ptNumber[MAX_PROCESS_COUNT];
  socklen_t nSizeOfSockMonitor,nSizeOfMonitorUdp;
  if(argc<5) {
    fprintf(stderr,"Pleasea give input like this\n %s ip_address port_number_tcp port_number_udp Log_File_Name\n",argv[0]);
    return 0;
  }
  fpWriteLog=fopen(argv[4],"w");
  createSocket(&nSocketFdMonitor,&sock_Monitor,argv[1],argv[2],TCP_CONNECTION,SERVER);
  createSocket(&nSocketFdMonitorUDP,&sock_MonitorUDP,argv[1],argv[3],UDP_CONNECTION,SERVER);
  nSizeOfSockMonitor=sizeof(sock_Monitor);

  if( (strPathOfExecutable=(char *)malloc(sizeof(char)*MAX_BUFFER))==NULL || (strConfigFile=(char **)malloc(sizeof(char *)*MAX_PROCESS_COUNT))==NULL || (strExecutableName=(char **)malloc(sizeof(char *)*MAX_PROCESS_COUNT))==NULL || (strStartNo=(char **)malloc(sizeof(char *)*MAX_PROCESS_COUNT))==NULL || (strPortServer=(char **)malloc(sizeof(char *)*MAX_PROCESS_COUNT))==NULL || (strEndNo=(char **)malloc(sizeof(char *)*MAX_PROCESS_COUNT))==NULL || (strDelayNextNumber=(char **)malloc(sizeof(char *)*MAX_PROCESS_COUNT))==NULL ) {
    fprintf(stderr,"Memory allocation failed\n");
    return 0;
  }

  for(i=0;i<MAX_PROCESS_COUNT;i++) {
    if((strStartNo[i]=(char *)malloc(sizeof(char)*MIN_STRING_LEN))==NULL || (strEndNo[i]=(char *)malloc(sizeof(char)*MIN_STRING_LEN))==NULL ||  (strDelayNextNumber[i]=(char *)malloc(sizeof(char)*MIN_STRING_LEN))==NULL || (strPortServer[i]=(char *)malloc(sizeof(char)*MIN_STRING_LEN))==NULL ) {
      fprintf(stderr,"Memory allocation failed\n");
      return 0;
    }
    nSocketFdClient[i]=accept(nSocketFdMonitor,(struct sockaddr *)&sock_Monitor,(socklen_t *)&nSizeOfSockMonitor);
    nNoByteGet=recv(nSocketFdClient[i],strBuffer,MAX_BUFFER,0);
    if(nNoByteGet<0) {
      fprintf(stderr,"errno %d errname:%s",errno,strerror(errno));
      return 0;
    }
    fprintf(stdout,"Recevie Intial:%s\n",strBuffer);
    sscanf(strBuffer,"%s %s %s %s",strStartNo[i],strEndNo[i],strDelayNextNumber[i],strPortServer[i]);
  }

  for(i=0;i<MAX_PROCESS_COUNT;i++) {
    createSocket(&nSocketFdSendUI[i],&sock_Client,argv[1],strPortServer[i],TCP_CONNECTION,CLIENT);
    if((strConfigFile[i]=(char *)malloc(sizeof(char)*MIN_STRING_LEN))==NULL || (strExecutableName[i]=(char *)malloc(sizeof(char)*MIN_STRING_LEN))==NULL ) {
      fprintf(stderr,"Memory allocation failed\n");
      return 0;
    }
    fscanf(stdin,"%s",strExecutableName[i]);
    strcpy(strConfigFile[i],strExecutableName[i]);
    strcpy(strConfigFile[i]+(int)strlen(strExecutableName[i]),".config");

    fpWrite=fopen(strConfigFile[i],"w");
    strcpy(strTemp,strExecutableName[i]);
    strcpy(strTemp+(int)strlen(strExecutableName[i]),".log");
    fprintf(fpWrite,"%s %s %s %s %s %s %s",LOCAL_HOST,argv[2],argv[3],strStartNo[i],strEndNo[i],strDelayNextNumber[i],strTemp);
    fflush(fpWrite);
    strPathOfExecutable=getcwd(strPathOfExecutable,MAX_BUFFER);
    strPathOfExecutable[(int)strlen(strPathOfExecutable)]='/';
    strcpy(strPathOfExecutable+(int)strlen(strPathOfExecutable),strExecutableName[i]);
    processInfo[i].strExecutableName=strExecutableName[i];
    processInfo[i].strPathOfExecutable=strPathOfExecutable;
    processInfo[i].strConfigFile=strConfigFile[i];
    processInfo[i].nSocketFdSendUI=nSocketFdSendUI[i];
    processInfo[i].nSocketFdMonitor=nSocketFdMonitor;
    processInfo[i].sock_Monitor=&sock_Monitor;
    processInfo[i].fpWriteLog=fpWriteLog;
    memset(&strTemp,0,strlen(strTemp));
    memset(&strPathOfExecutable,0,MAX_BUFFER);
  }

  for(i=0;i<MAX_PROCESS_COUNT;i++) {
    pthread_create(&ptNumber[i],NULL,executeProcess,&processInfo[i]);
    sleep(1);
  }
  /* monitor work and monitoring work */
  while(1) {
    memset(&strBuffer,0,strlen(strBuffer));
    nSizeOfMonitorUdp=sizeof(sock_MonitorUDP);
    nNoByteGet=recvfrom(nSocketFdMonitorUDP,strBuffer,MAX_BUFFER,0,(struct sockaddr *)&sock_MonitorUDP,(socklen_t *)&nSizeOfMonitorUdp);
    if(nNoByteGet<0) {
      fprintf(stderr,"WM:errno %d errname:%s",errno,strerror(errno));
      return 0;
    }
    fprintf(stdout,"Recevie Intial:%s\n",strBuffer);
  }

  return 0;
}
