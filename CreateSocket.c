# include "monitor.h"

int createSocket(int *nSocketFdStore,struct sockaddr_in *sockaddr_in_Store,char *strIPAddress,char *strPort,char cType,char cFlag)
{
  int nSockfd=-1;
  struct sockaddr_in sockaddr_in_StoreTemp;
  if(cType==TCP_CONNECTION ) {
  nSockfd = socket(PF_INET, SOCK_STREAM, 0);
  if(nSockfd<0) { 
    fprintf (stderr,"%d %s\n",errno,strerror(errno));
    return 0;
  }
  memset(&sockaddr_in_StoreTemp,0,sizeof(sockaddr_in_StoreTemp));
  sockaddr_in_StoreTemp.sin_family = AF_INET;
  sockaddr_in_StoreTemp.sin_port = htons(atoi(strPort));
  sockaddr_in_StoreTemp.sin_addr.s_addr = inet_addr(strIPAddress);
  memset(sockaddr_in_StoreTemp.sin_zero, '\0', sizeof sockaddr_in_StoreTemp.sin_zero);  
  if(cFlag==CLIENT)
  connect(nSockfd, (struct sockaddr *) &sockaddr_in_StoreTemp, sizeof(sockaddr_in_StoreTemp));
  }
  else if (cType==UDP_CONNECTION ) {
   nSockfd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if(nSockfd<0) { 
    fprintf (stderr,"%d %s\n",errno,strerror(errno));
    return 0;
  }
  memset(&sockaddr_in_StoreTemp,0,sizeof(sockaddr_in_StoreTemp));
  sockaddr_in_StoreTemp.sin_family = AF_INET;
  sockaddr_in_StoreTemp.sin_port = htons(atoi(strPort));
  sockaddr_in_StoreTemp.sin_addr.s_addr = inet_addr(strIPAddress);
  memset(sockaddr_in_StoreTemp.sin_zero, '\0', sizeof sockaddr_in_StoreTemp.sin_zero);  
  }
  if((cType==TCP_CONNECTION || cType==UDP_CONNECTION) && cFlag==SERVER) { 
  if(bind(nSockfd, (struct sockaddr *) &sockaddr_in_StoreTemp, sizeof(sockaddr_in_StoreTemp))<0) {
    fprintf (stderr,"Bind Error:%c:%c:%s %d %s\n",cType,cFlag,strPort,errno,strerror(errno));
    return 0;
  }
  }
  if(cType==TCP_CONNECTION && cFlag==SERVER)  {
  if(listen(nSockfd,5)<0) {
    fprintf (stderr,"%d %s\n",errno,strerror(errno));
    return 0;
  }
  }

  *nSocketFdStore=nSockfd;
  *sockaddr_in_Store=sockaddr_in_StoreTemp;
  return 0;
}
