# include "monitor.h"

int relaunchProcess(Process_Info processInfo,Process_Info *newProcess,FILE *fpWriteLog,int nSocketFdMonitor,int nSocketFdSendPrinter)
{
  int nProcessId,nPid,nSocketFdClient,nNoByteGet;
  char strBuffer[MAX_BUFFER];
  struct sockaddr_in sock_Monitor;
  Process_Info process;
  socklen_t nSizeOfSockMonitor;
  fprintf(stdout,"%s %s %s\n",processInfo.strPathOfExecutable,processInfo.strCommadToExecute,processInfo.strConfigFile);
  strcpy(process.strPathOfExecutable,processInfo.strPathOfExecutable);
  strcpy(process.strCommadToExecute,processInfo.strCommadToExecute);
  strcpy(process.strConfigFile,processInfo.strConfigFile);
  if((nProcessId=fork())==0) {
    execlp(processInfo.strPathOfExecutable,processInfo.strCommadToExecute,processInfo.strConfigFile,NULL);
    exit(1);
  }
  process.nPid=nProcessId;
  fprintf(stdout,"PID %s:%d\n",processInfo.strCommadToExecute,nProcessId);
  fprintf(fpWriteLog,"203 PROCESS_LUNCH %s %d\n",processInfo.strCommadToExecute,nProcessId);
  fflush(fpWriteLog);

  memset(&strBuffer,0,MAX_BUFFER);
  nSizeOfSockMonitor=sizeof(sock_Monitor);
  nSocketFdClient=accept(nSocketFdMonitor,(struct sockaddr *)&sock_Monitor,(socklen_t *)&nSizeOfSockMonitor);
  if((nPid=fork())==0) {
    while(1) {
      nNoByteGet=recv(nSocketFdClient,strBuffer,MAX_BUFFER,0);
      if(send(nSocketFdSendPrinter,strBuffer,MAX_BUFFER,0)<0) {
        fprintf(fpWriteLog,"404 SEND_ERROR %d %s\n",errno,strerror(errno));
        fflush(fpWriteLog);
        exit(0);
      }
      if(nNoByteGet>0)
        fprintf(fpWriteLog,"200 SEND_PRINTER %s %d %d\n",strBuffer,nSocketFdSendPrinter,nSocketFdClient);
      fflush(fpWriteLog);
      memset(&strBuffer,0,MAX_BUFFER);
    }
  }
  process.nPidKill=nPid;
  *newProcess=process;
 return 0;
}
