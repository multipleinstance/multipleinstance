#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>


/* CONSTANT VALUE */
# define SERVER 'S'
# define CLIENT 'C'
# define TCP_CONNECTION 'T'
# define UDP_CONNECTION 'U'
# define MIN_STRING_LEN 30
# define MAX_ARG_COUNT 3
# define MAX_BUFFER 1024
# define MAX_CHARS_IN_LINE 80
# define MAX_TOKEN_COUNT 10
# define MAX_PROCESS_COUNT 5 
# define LOCAL_HOST "127.0.0.1"
# define CONFIG_FILE_NAME(strPath) "'strPath'.config"

typedef struct process_info
{
  int nPid,nPidKill,nSocketFdMonitor,nSocketFdSendUI;
  char *strPathOfExecutable,*strCommadToExecute,*strConfigFile,*strExecuteOnConsole,*strExecutableName;
  struct sockaddr_in *sock_Monitor;
  FILE *fpWriteLog;
}Process_Info;

typedef struct thread_info
{
  int nSocketFd,nSleepAlive;
  struct sockaddr_in *sockUdp;
  socklen_t nSizeofSockUdp;
  char strMsg[MIN_STRING_LEN];
}ThreadInfo;


int createSocket(int *nSocketFdStore,struct sockaddr_in *sockaddr_in_Store,char *strIPAddress,char *strPort,char cType,char cFlag);
int tokenise(char *strBuffer,char ***strToken,int *nTokenCount);
int checkLessNumberSend(FILE *fpRead,int nNumber,int *nSizeOfFileOld,int *nSizeOfFile);
int integerToString(int nNumber,char **strStoreAns);
FILE * readFile(char *strPath,FILE *fpReadFile,int *nNewFileSize);
int writeLog(FILE *fpWrite,char strMsgCode[MIN_STRING_LEN],char strMsgName[MIN_STRING_LEN],char strSendMsg[MAX_BUFFER],char strComment[MAX_BUFFER]);
int signalHandling(int nPidMonitor,int nPidKill,int nStatus,FILE *fpWriteLog);
int relaunchProcess(Process_Info processInfo,Process_Info *newProcess,FILE *fpWriteLog,int nSocketFdMonitor,int nSocketFdSendUI);
void * aliveSendMsg(void *ptInfo);
void * executeProcess(void *arg);
