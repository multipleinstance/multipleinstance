# include "monitor.h"

int main(int argc,char **argv)
{
  int nSocketFdPrinter,nSocketFdClient,i,nNoByteGet;
  char strArgumentPass[MAX_ARG_COUNT][MIN_STRING_LEN],strBuffer[MAX_BUFFER];
  struct sockaddr_in sock_Printer;
  socklen_t nSizeOfSockPrinter;
  memset(strBuffer,0,MAX_BUFFER);
  if(argc<4)
  {
    fprintf(stderr,"Pleasea give input like this\n ./a.out ip_address port_number_self port_number_monitor\n");
    return 0;
  }
  for(i=0;i<MAX_ARG_COUNT;i++) {
    fscanf(stdin,"%s",strArgumentPass[i]);
    strcpy(strBuffer+(int)strlen(strBuffer),strArgumentPass[i]);
    strBuffer[(int)strlen(strBuffer)]=' ';
  }
  strcpy(strBuffer+(int)strlen(strBuffer),argv[2]);

  createSocket(&nSocketFdPrinter,&sock_Printer,argv[1],argv[3],TCP_CONNECTION,CLIENT);
    if(send(nSocketFdPrinter,strBuffer,MAX_BUFFER,0)<0) {
    fprintf (stderr,"%d %s\n",errno,strerror(errno));
    return 0;
  }
  memset(&sock_Printer,0,sizeof(sock_Printer));
  createSocket(&nSocketFdPrinter,&sock_Printer,argv[1],argv[2],TCP_CONNECTION,SERVER);
  nSizeOfSockPrinter=sizeof(sock_Printer);
  nSocketFdClient=accept(nSocketFdPrinter,(struct sockaddr *)&sock_Printer,&nSizeOfSockPrinter);
  while(1)
  {
    nNoByteGet=recv(nSocketFdClient,strBuffer,MAX_BUFFER,0);
    if(nNoByteGet>0)
      fprintf(stdout,"%s\n",strBuffer);
    if(strcmp(strBuffer,strArgumentPass[1])==0)
      break;
    memset(&strBuffer,0,MAX_BUFFER);
  }

  return 0;
}
