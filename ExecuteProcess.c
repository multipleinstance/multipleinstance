#  include "monitor.h"

void * executeProcess(void *arg)
{
  int nPid=0,nSocketFdClient,nNoByteGet;
  char strBuffer[MAX_BUFFER],strTemp[MIN_STRING_LEN];
  socklen_t nSizeOfSockMonitor;
  Process_Info *pInfo=arg;
  strTemp[0]='.';strTemp[1]='/';
  strcpy(strTemp+2,pInfo->strExecutableName);
  fprintf(stdout,"%s %s %s %s\n",pInfo->strPathOfExecutable,pInfo->strExecutableName,pInfo->strConfigFile,strTemp);
  if((nPid=fork())==0) {
    execlp(pInfo->strPathOfExecutable,strTemp,pInfo->strConfigFile,NULL);
    exit(1);
  }
  fprintf(stdout,"PID %s:%d\n",pInfo->strExecutableName,nPid);
  fprintf(pInfo->fpWriteLog,"203 PROCESS_LUNCH %s %d\n",pInfo->strExecutableName,nPid);
  fflush(pInfo->fpWriteLog);

  memset(&strBuffer,0,MAX_BUFFER);
  nSizeOfSockMonitor=sizeof(&pInfo->sock_Monitor);
  nSocketFdClient=accept(pInfo->nSocketFdMonitor,(struct sockaddr *)(&pInfo->sock_Monitor),(socklen_t *)&nSizeOfSockMonitor);
  if(nSocketFdClient<0) {
    fprintf(pInfo->fpWriteLog,"405 ERROR_SOCKET %d %s\n",errno,strerror(errno));
    fflush(pInfo->fpWriteLog);
  }
  while(1) {
    nNoByteGet=recv(nSocketFdClient,strBuffer,MAX_BUFFER,0);
    if(send(pInfo->nSocketFdSendUI,strBuffer,MAX_BUFFER,0)<0) {
      fprintf(pInfo->fpWriteLog,"404 SEND_ERROR %d %s\n",errno,strerror(errno));
      fflush(pInfo->fpWriteLog);
      //exit(0);
      return ((void *)0);
    }
    if(nNoByteGet>0)
      fprintf(pInfo->fpWriteLog,"200 SEND_PRINTER %s %d %d\n",strBuffer,pInfo->nSocketFdSendUI,nSocketFdClient);
    fflush(pInfo->fpWriteLog);
    memset(&strBuffer,0,MAX_BUFFER);
  }

  return ((void *)0);
}
